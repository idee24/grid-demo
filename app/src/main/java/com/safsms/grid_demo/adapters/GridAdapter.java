package com.safsms.grid_demo.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.safsms.grid_demo.R;
import com.safsms.grid_demo.activities.MainActivity;
import com.safsms.grid_demo.models.Animals;

import java.util.LinkedList;

public class GridAdapter extends RecyclerView.Adapter<GridAdapter.GridViewHolder> {

    private LayoutInflater inflater;
    private Animals animals;
    private LinkedList<Animals> animalList;
    private Context context;


    public GridAdapter(Context context, LinkedList<Animals> animalList){
        inflater = LayoutInflater.from(context);
        this.animals = animals;
        this.animalList = animalList;
    }

    static class GridViewHolder extends RecyclerView.ViewHolder{

        private TextView imageTitleTextView;
        private ImageView imageItem;

        public GridViewHolder(View itemView) {
            super(itemView);

            imageTitleTextView = itemView.findViewById(R.id.imageTitleTextView);
            imageItem = itemView.findViewById(R.id.image_item);
        }

        public void setData(Context context, Animals current, int position) {

            this.imageItem.setImageResource(current.getResourceID());
            this.imageTitleTextView.setText(current.getTitle());
        }
    }

    @NonNull
    @Override
    public GridViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.grid_item, parent, false);
        return new GridViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull GridViewHolder holder, int position) {
        Animals current = animalList.get(position);
        holder.setData(context, current, position);
    }

    @Override
    public int getItemCount() {
        return animalList.size();
    }

}
