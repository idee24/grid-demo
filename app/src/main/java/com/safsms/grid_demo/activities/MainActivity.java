package com.safsms.grid_demo.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.safsms.grid_demo.R;
import com.safsms.grid_demo.adapters.GridAdapter;
import com.safsms.grid_demo.models.Animals;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        refreshRecyclerView();

        if (getSupportActionBar() != null) {

            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_name);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Photos");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){

            case R.id.delete:
                showToast("Delete Selected");
                break;

            case R.id.edit:
                transitionToImageDemo();
                break;

            case android.R.id.home:
                showToast("Home Selected");
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void transitionToImageDemo() {
        Intent intent = new Intent(MainActivity.this, ImageDemoActivity.class);
        startActivity(intent);
        finish();
    }

    private void showToast(String action) {

        Toast.makeText(this, action, Toast.LENGTH_SHORT).show();
    }

    public void refreshRecyclerView(){

        RecyclerView gridRecyclerView = findViewById(R.id.recycler_view);
        GridAdapter adapter = new GridAdapter(this, Animals.getData());
        gridRecyclerView.setAdapter(adapter);

        StaggeredGridLayoutManager layoutManager =
                new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        gridRecyclerView.setLayoutManager(layoutManager);
        gridRecyclerView.setItemAnimator(new DefaultItemAnimator());
    }


}
