package com.safsms.grid_demo.activities;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import com.safsms.grid_demo.R;

import java.util.ArrayList;
import java.util.List;

public class ImageDemoActivity extends AppCompatActivity {

    private ImageView logoImageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_demo);

        logoImageView = findViewById(R.id.logoImageView);
        switchImage();
    }

    private void switchImage(){
        final Handler handler = new Handler();
        final List<Integer> index = new ArrayList<>();
        index.add(0, 0);
        final int[] imageResources = {R.drawable.loader_1, R.drawable.loader_2, R.drawable.loader_3};
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                logoImageView.setImageResource(imageResources[index.get(0)]);
                index.set(0, (index.get(0) + 1) % imageResources.length);
                handler.postDelayed(this, 850);
            }
        }, 850);
    }


}
